#include "stack.h"
#include "ijvm.h"
#include <stdbool.h>
#include <stdlib.h>

//Difne the possible errors globaly since we will use it more than once
#define ERR_EMPTY "Stack is empty!"
#define ERR_FULL "Stack is full!"

/*
-- This function is used to push/add an element to teh stack
-- The element will be pushed/added to the top of the stack
*/
void push(word_t word)
{
  if (isFull())
  {
    fprintf(stderr, "%s\n", ERR_FULL);
  }
  else
  {
    curStack->sp++;
    curStack->stackData[curStack->sp] = word;
  }
}

/*
-- This function is used to pop/remove an element from stack
-- The element will be popped/removed from the top of the stack
-- Poped/removed element will be returned to the user
*/
word_t pop() 
{
  if (!isEmpty())
  {
    word_t popedItem = curStack->stackData[getStackPointer()];
    curStack->sp--;
    return popedItem;
  }
  else
  {
    fprintf(stderr, "%s\n", ERR_EMPTY);
    return -1;
  }
}

/*
-- This function is used to peek at the top element of the stack
-- The element will be returned to the user but NOT removed from the stack
*/
word_t peek()
{
  if (!isEmpty())
  {
    word_t popedItem = curStack->stackData[getStackPointer()];
    return popedItem;
  }
  else
  {
    fprintf(stderr, "%s\n", ERR_EMPTY);
    return -1;
  }
}

/*
-- This function will return the size of the current stack
*/
int getStackSize()
{
  return getStackPointer() - getFramePointer();
}

/*
-- This function will return TRUE if the stack is empty
*/
bool isEmpty()
{
  return getFramePointer() == getStackPointer();
}

/*
-- This function will return TRUE if the stack is full
*/
bool isFull(void)
{
  return getStackPointer() == STACK_SIZE;
}


/*
-- This function stores the program counter in its stack
*/
void pushProgramCounter(word_t word)
{
  if (getAnyStackPointer(pcStack) == STACK_SIZE)
  {
    fprintf(stderr, "%s\n", ERR_FULL);
  }
  else
  {
    pcStack->sp++;
    pcStack->stackData[getAnyStackPointer(pcStack)] = word;
  }
}

/*
-- This function pops the program counter from its stack
*/
word_t popProgramCounter()
{
  if (getAnyFramePointer(pcStack) != getAnyStackPointer(pcStack))
  {
    word_t popedItem = pcStack->stackData[getAnyStackPointer(pcStack)];
    pcStack->sp--;
    return popedItem;
  }
  else
  {
    fprintf(stderr, "%s\n", ERR_EMPTY);
    return -1;
  }
}

/*
-- This function stores the frame pointer in its stack
*/
void pushFramePointer(word_t word)
{
  if (getAnyStackPointer(lvStack) == STACK_SIZE)
  {
    fprintf(stderr, "%s\n", ERR_FULL);
  }
  else
  {
    lvStack->sp++;
    lvStack->stackData[getAnyStackPointer(lvStack)] = word;
  }
}

/*
-- This function pops the frame pointer from its stack
*/
word_t popFramePointer()
{
  if (getAnyFramePointer(lvStack) != getAnyStackPointer(lvStack))
  {
    word_t popedItem = lvStack->stackData[getAnyStackPointer(lvStack)];
    lvStack->sp--;
    return popedItem;
  }
  else
  {
    fprintf(stderr, "%s\n", ERR_EMPTY);
    return -1;
  }
}

/*
-- This function return the current stack stack pointer
*/
int getStackPointer()
{
  return curStack->sp;
}

/*
-- This function return any stack pointer
*/
int getAnyStackPointer(Stack* stackArg)
{
  return stackArg->sp;
}

/*
-- This function return the curStack frame pointer
*/
int getFramePointer()
{
  return curStack->lv;
}

/*
-- This function return any frame pointer
*/
int getAnyFramePointer(Stack* stackArg)
{
  return stackArg->lv;
}

/*
-- This function return the stack data of the current stack
*/
word_t* getStack()
{
  return curStack->stackData;
}

/*
-- This function return the stack data of any stack
*/
word_t* getAnyStack(Stack* stackArg)
{
  return stackArg->stackData;
}




