#include <ijvm.h>
#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>
#include "stack.h"
#include "instructions.h"

typedef struct DataBlock {
  word_t dataOrigin;
  word_t dataSize;
  byte_t* data;
} DataBlock;

typedef struct ConstantBlock {
  word_t constOrigin;
  word_t constSize;
  word_t* constants;
} ConstantBlock;

FILE* file;
DataBlock dataBlock;
ConstantBlock constBlock;

static uint32_t swap_uint32(uint32_t num)
{
  return ((num >> 24) & 0xff) | ((num << 8) & 0xff0000) | ((num >> 8) & 0xff00) | ((num << 24) & 0xff000000);
}


//Check if first block contains magic word
bool isMagicWord()
{
  word_t magicWord;
  fread(&magicWord, sizeof(word_t), 1, file);
  magicWord = swap_uint32(magicWord);
  if (magicWord != MAGIC_NUMBER)
  {
    return false;
  }
  else
  {
    return true;
  }
}

//Read next word
word_t getWord()
{
  word_t word;
  fread(&word, sizeof(word_t), 1, file);
  word = swap_uint32(word);

  return word;
}

//Fill the DataBlock and ConstantBlock structs
void createBlocks()
{
  constBlock.constOrigin = getWord(file);
  constBlock.constSize = getWord(file);
  if (constBlock.constSize > 0)
  {
    int constSize = (int)constBlock.constSize;
    constBlock.constants = malloc(constSize);
    constSize = constSize/4;
    fread(constBlock.constants, sizeof(word_t), constSize, file);
  }

  dataBlock.dataOrigin = getWord(file);
  dataBlock.dataSize = getWord(file);
  if (dataBlock.dataSize > 0)
  {
    int textSize = (int)dataBlock.dataSize;
    dataBlock.data = malloc(textSize);
    fread(dataBlock.data, sizeof(byte_t), textSize, file);
  }
}

void initStacks()
{
  curStack = calloc(1, sizeof(Stack));
  //Create some space for local varioables. From framepointer to 125...
  curStack->sp = 125;
  curStack->lv = 0;

  lvStack = calloc(1, sizeof(Stack));
  pcStack = calloc(1, sizeof(Stack));
}

int init_ijvm(char* binary_file)
{
  //Open the binary file (passed as argument) 
  file = fopen(binary_file, "rb");

  //Initialize and alocate memory for the stacks used in the solution
  initStacks();

  //Initialize the files used in the solution
  fileIn = stdin;
  fileOut = stdout;

  //Check if the magic word is there, if not exit the program
  if (isMagicWord(file))
  {
    createBlocks(file);
    return 0;
  }
  else
  {
    return -1;
  }
}

void destroy_ijvm()
{
  // Reset IJVM state and free all of the used memoory for the stack
  free(dataBlock.data);
  free(constBlock.constants);
  free(curStack);
  free(lvStack);
  free(pcStack);
  pCounter = 0;
  fclose(file);
  
}


void set_input(FILE * fp)
{
  fileIn = fp;
}

void set_output(FILE * fp)
{
  fileOut = fp;
}

/**
 * Returns the value of the program counter (as an offset from the first instruction).
 **/
int get_program_counter() {
  return pCounter;
}

/**
 * @return The value of the current instruction represented as a byte_t.
 *
 * This should NOT increase the program counter.
 **/
byte_t get_instruction()
{
  return dataBlock.data[pCounter];
}

/**
 * Step (perform) one instruction and return.
 * In the case of WIDE, perform the whole WIDE_ISTORE or WIDE_ILOAD.
 * Returns true if an instruction was executed. Returns false if machine has
 * halted or encountered an error.
 **/
bool step()
{
  bool completed = true;
  switch (get_instruction()) {
  case OP_BIPUSH:
    inst_bipush();
    break;
  case OP_DUP:
    inst_dup();
    break;
  case OP_ERR:
    completed = inst_err();
    break;
  case OP_GOTO:
    inst_goto();
    break;
  case OP_HALT:
    completed = inst_halt();
    break;
  case OP_IADD:
    inst_iadd();
    break;
  case OP_IAND:
    inst_iand();
    break;
  case OP_IFEQ:
    inst_ifeq();
    break;
  case OP_IFLT:
    inst_iflt();
    break;
  case OP_ICMPEQ:
    inst_if_icmpeq();
    break;
  case OP_IINC:
    inst_iinc();
    break;
  case OP_ILOAD:
    inst_iload();
    break;
  case OP_IN:
    inst_in();
    break;
  case OP_INVOKEVIRTUAL:
    inst_invokevirtual();
    break;
  case OP_IOR:
    inst_ior();
    break;
  case OP_IRETURN:
    inst_ireturn();
    break;
  case OP_ISTORE:
    inst_istore();
    break;
  case OP_ISUB:
    inst_isub();
    break;
  case OP_LDC_W:
    inst_ldc_w();
    break;
  case OP_NOP:
    inst_nop();
    break;
  case OP_OUT:
    inst_out();
    break;
  case OP_POP:
    inst_pop();
    break;
  case OP_SWAP:
    inst_swap();
    break;
  case OP_WIDE:
    inst_wide();
    break;
  default:
    completed = false;
  }
  pCounter++;
  return completed;
}


/**
 * Returns the currently loaded program text as a byte array.
 **/
byte_t* get_text()
{
  return dataBlock.data;
}


/**
 * Returns the size of the currently loaded program text.
 **/
int text_size()
{
  return dataBlock.dataSize;
}

/**
 * Run the vm with the current state until the machine halts.
 **/
void run()
{
  // Step while you can
  int instuctionSetSize = text_size();
  while (pCounter < instuctionSetSize)
  {
    bool res = step();
    if (res == false)
      break;
  }
}


/**
 * @param i, index of variable to obtain.
 * @return Returns the i:th local variable of the current frame.
 **/
word_t get_local_variable(int num)
{
  return curStack->stackData[curStack->lv + num];
}

/**
 * This function should return the word at the top of the stack of the current
 * frame, interpreted as a signed integer.
 **/
word_t tos()
{
  return peek();
}


/**
 * @param i, index of the constant to obtain
 * @return The constant at location i in the constant pool.
 **/
word_t get_constant(int i)
{
  return swap_uint32(constBlock.constants[i]);
}

/**
 * Check whether the machine has any more instructions to execute.
 *
 * A machine will stop running after:
 * - reaching the end of the text section
 * - encountering either the HALT/ERR instructions
 * - encountering an invalid instruction
 */
bool finished()
{
  if (get_instruction() == OP_HALT || pCounter == dataBlock.dataSize || step() == false)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/**
 * Returns the stack of the current frame as an array of integers,
 * with entry[0] being the very bottom of the stack and
 * entry[stack_size() - 1] being the top.
 **/
word_t* get_stack()
{
  return getStack();
}

/**
 * Returns the size of the stack of the current frame.
 **/
int stack_size()
{
  return getStackSize();
}





