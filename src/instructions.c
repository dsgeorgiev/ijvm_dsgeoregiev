#include "ijvm.h"
#include "stack.h"
#include "instructions.h"
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>


short readShort()
{
  pCounter++;
  byte_t right = get_instruction();
  pCounter++;
  byte_t left = get_instruction();
  //make left shits since we need a short (8 bytes)
  short location = ((right << 8) | (left));
  return location;
}


/*
-- Push a byte onto stack
*/
void inst_bipush()
{
  //printf("BIPUSH\n");
  pCounter++;
  int8_t num = get_instruction();
  push(num);
}

/*
-- Copy peek word on stack and push onto stack
*/
void inst_dup()
{
  //printf("DUP\n");
  word_t word = peek();
  push(word);
}

/*
-- Print an error message and halt the simulator
*/
bool inst_err()
{
  fprintf(stderr, "%s\n", "Error occured\n");
  return false;
}

/*
-- Unconditional jump
*/
void inst_goto()
{
  //printf("GOTO\n");
  short jumpToLocation = readShort();
  pCounter += (jumpToLocation - 3); // -3 since we have 3 extra bits 
}

/*
-- Halt the simulator
*/
bool inst_halt()
{
  //printf("HALT\n");
  return false;
}

/*
-- Pop two words from stack; push their sum
*/
void inst_iadd()
{
  //printf("IADD\n");

  word_t x = pop();
  word_t y = pop();

  word_t res = x + y;

  push(res);
}

/*
-- Pop two words from stack; push bit-wise AND
*/
void inst_iand()
{
  //printf("IAND\n");
  word_t x = pop();
  word_t y = pop();

  word_t res = x & y;

  push(res);
}

/*
-- Pop word from stack and branch if it is zero
*/
void inst_ifeq()
{
  //printf("IFEQ\n");
  if (pop() == 0)
  {
    short jumpToLocation = readShort();
    pCounter += (jumpToLocation - 3); // -3 since we have 3 extra bits 
  }
  else 
  {
    pCounter += 2; //skip both instruction and value after
  }
}

/*
-- Pop word from stack and branch if it is less than zero
*/
void inst_iflt()
{
  //printf("IFLT\n");
  if (pop() < 0)
  {
    short jumpToLocation = readShort();
    pCounter += (jumpToLocation - 3); // -3 since we have 3 extra bits 
  }
  else 
  {
    pCounter += 2; //skip both instruction and value after
  }
}

/*
-- Pop two words from stack and branch if they are equal
*/
void inst_if_icmpeq()
{
  //printf("IF_ICMPEQ\n");
  word_t x = pop();
  word_t y = pop();
  if (x == y)
  {
    short jumpToLocation = readShort();
    pCounter += (jumpToLocation - 3); // -3 since we have 3 extra bits 
  }
  else 
  {
    pCounter += 2; //skip both instruction and value after
  }
}

/*
-- Add a constant value to a local variable. The first byte is the variable index. The second byte is the constant
*/
void inst_iinc()
{
  //printf("IINC\n");

  pCounter++;
  byte_t var = get_instruction();
  pCounter++;
  int8_t constant = (int8_t)get_instruction();
  curStack->stackData[getFramePointer() + var] = curStack->stackData[getFramePointer() + var] + constant;
}

/*
-- Push local variable onto stack
*/
void inst_iload()
{
  //printf("ILOAD\n");

  pCounter++;
  byte_t varAddress = get_instruction();
  push(get_local_variable(varAddress));
}

/*
-- Reads a character from the input and pushes it onto the stack. If no character is available, 0 is pushed
*/
void inst_in()
{
  //printf("IN\n");
  char inChar = 0;
  fscanf(fileIn, "%c", &inChar);

  push(inChar);
}

/*
-- Invoke a method, pops object reference and pops arguments from stack.
*/
void inst_invokevirtual()
{
  //printf("INVOKEVIRTUAL\n");
  // Get the address of the method
  unsigned short methodAddress = (unsigned)readShort();
  // Get the method that is at this address
  word_t method = get_constant(methodAddress);
  // Keep the pCounter safe
  pushProgramCounter(pCounter); 
  // Change the place of the program counter to start from where the method is staring 
  pCounter = method - 1;
  // Get the numbger of argument of teh method
  short args = readShort();
  // the size of the local variablers for the method
  unsigned short varSize = (unsigned)readShort();

  //Keep the frame pointer safe
  pushFramePointer(getFramePointer());
  //Adjusat the frame pointer to start at the beggining of the method
  curStack->lv = getStackPointer() - args + 1;
  //Adjust the stack piointet to start right after the local variables
  curStack->sp = getStackPointer() + varSize;
}

/*
-- Pop two words from stack; push bit-wise OR. Note: the book uses the opcode 0x80.
*/
void inst_ior()
{
  //printf("IOR\n");
  word_t x = pop();
  word_t y = pop();

  word_t res = x | y;

  push(res);
}

/*
-- Return from method with a word value
*/
void inst_ireturn()
{
  //printf("IRETURN\n");

  //get the top which will be the result of teh INVOKEVIRTUAL
  word_t value = peek();

  //change back teh stack poiner to point at the old PEEK
  curStack->sp = getFramePointer() - 1;
  //change back the frame pointer (pop it from its stack)
  curStack->lv = popFramePointer();
  //push teh result to the current stack
  push(value);
  //change back the fprogram counter (pop it from its stack)
  pCounter = popProgramCounter();
}

/*
-- Pop word from stack and store in local variable
*/
void inst_istore()
{
  //printf("ISTORE\n");

  pCounter++;
  byte_t varAddress = get_instruction();
  //Storing the local variable to the momory wiht position varAddress
  curStack->stackData[getFramePointer() + varAddress] = pop();
}

/*
-- Pop two words from stack; subtract the peek word from the second to peek word, push the answer;
*/
void inst_isub()
{
  //printf("ISUB\n");

  word_t x = pop();
  word_t y = pop();

  word_t res = y - x;

  push(res);
}

/*
-- Push constant from constant pool onto stack
*/
void inst_ldc_w()
{
  //printf("LDC_W\n");

  unsigned short constVar = (unsigned) readShort();
  word_t constant = get_constant(constVar);
  push(constant);
}

/*
-- Do nothing
*/
void inst_nop()
{
  //printf("NOP\n");
}

/*
-- Pop word off stack and print it to standard out
*/
void inst_out()
{
  //printf("IOUT\n");
  int result = pop();
  fprintf(fileOut, "%c", result);
}

/*
-- Delete word from peek of stack
*/
void inst_pop()
{
  //printf("POP\n");
  pop();
}

/*
-- Swap the two peek words on the stack
*/
void inst_swap()
{
  //printf("SWAP\n");

  word_t x = pop();
  word_t y = pop();

  push(x);
  push(y);
}

/*
-- Pop word from stack and store in local variable ***WIDE***
*/
void inst_istore_wide()
{
  //printf("ISTORE\n");
  unsigned short varAddress = (unsigned)readShort();
  curStack->stackData[getFramePointer() + varAddress] = pop();
}

/*
-- Add a constant value to a local variable. The first byte is the variable index. The second byte is the constant ***WIDE***
*/
void inst_iinc_wide()
{
  //printf("IINC\n");
  unsigned short varAddress = (unsigned)readShort();
  pCounter++;
  int8_t constant = (int8_t)get_instruction();
  curStack->stackData[getFramePointer() + varAddress] = curStack->stackData[getFramePointer() + varAddress] + constant;
}

/*
-- Push local variable onto stack ***WIDE***
*/
void inst_iload_wide()
{
  //printf("ILOAD\n");
  unsigned short varAddress = (unsigned)readShort();
  push(get_local_variable(varAddress));
}

/*
-- Prefix instruction; next instruction has a 16-bit index
*/
void inst_wide()
{
  //printf("WIDE\n");
  pCounter++;
  switch (get_instruction()) {
  case OP_ILOAD:
    inst_iload_wide();
    break;
  case OP_ISTORE:
    inst_istore_wide();
    break;
  case OP_IINC:
    inst_iinc_wide();
    break;
  default:
    inst_err();
  }
}


