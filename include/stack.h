#include "ijvm.h"
#include <stdlib.h>

#define STACK_SIZE 60000

typedef struct Stack {
  word_t stackData[STACK_SIZE];
  int sp;
  int lv;
} Stack;

Stack* curStack;
Stack* pcStack;
Stack* lvStack;

/*
-- This function is used to push/add an element to teh stack
-- The element will be pushed/added to the top of the stack
*/
void push(word_t);

/*
-- This function is used to pop/remove an element from stack
-- The element will be popped/removed from the top of the stack
-- Poped/removed element will be returned to the user
*/
word_t pop(void);

/*
-- This function is used to peek at the top element of the stack
-- The element will be returned to the user but NOT removed from the stack
*/
word_t peek(void);

/*
-- This function will return the size of the current stack
*/
int getStackSize(void);

/*
-- This function will return TRUE if the stack is empty
*/
bool isEmpty(void);

/*
-- This function will return TRUE if the stack is full
*/
bool isFull(void);

/*
-- This function stores the program counter in its stack
*/
void pushProgramCounter(word_t);

/*
-- This function pops the program counter from its stack
*/
int popProgramCounter(void);

/*
-- This function stores the frame pointer in its stack
*/
void pushFramePointer(word_t);

/*
-- This function pops the frame pointer from its stack
*/
int popFramePointer(void);

/*
-- This function return the curent stack stack pointer
*/
int getStackPointer(void);

/*
-- This function return any stack pointer
*/
int getAnyStackPointer(Stack* stackArg); 

/*
-- This function return the curent stack frame pointer
*/
int getFramePointer(void);

/*
-- This function return any frame pointer
*/
int getAnyFramePointer(Stack* stackArg);

/*
-- This function return the stack data of the curent stack
*/
word_t* getStack(void);

/*
-- This function return the stack data of any stack
*/
word_t* getAnyStack(Stack* stackArg);
