/*
-- Creating both files that wiil be used for input and output
*/
FILE* fileIn;
FILE* fileOut;

/*
-- Initialize a function that will read a short used for several instructions
*/
short readTwoBytes(void);

/*
-- Initialize program counter that will be accessed from both:
-- machine.c and instructions.c in order to sync them
*/
int pCounter;

/*
-- Push a byte onto stack
*/
void inst_bipush(void);

/*
-- Copy top word on stack and push onto stack
*/
void inst_dup(void);

/*
-- Print an error message and halt the simulator
*/
bool inst_err(void);

/*
-- Unconditional jump
*/
void inst_goto(void);

/*
-- Halt the simulator
*/
bool inst_halt(void);

/*
-- Pop two words from stack; push their sum
*/
void inst_iadd(void);

/*
-- Pop two words from stack; push bit-wise AND
*/
void inst_iand(void);

/*
-- Pop word from stack and branch if it is zero
*/
void inst_ifeq(void);

/*
-- Pop word from stack and branch if it is less than zero
*/
void inst_iflt(void);

/*
-- Pop two words from stack and branch if they are equal
*/
void inst_if_icmpeq(void);

/*
-- Add a constant value to a local variable. The first byte is the variable index. The second byte is the constant
*/
void inst_iinc(void);

/*
-- Push local variable onto stack
*/
void inst_iload(void);

/*
-- Reads a character from the input and pushes it onto the stack. If no character is available, 0 is pushed
*/
void inst_in(void);

/*
-- Invoke a method, pops object reference and pops arguments from stack.
*/
void inst_invokevirtual(void);

/*
-- Pop two words from stack; push bit-wise OR. Note: the book uses the opcode 0x80.
*/
void inst_ior(void);

/*
-- Return from method with a word value
*/
void inst_ireturn(void);

/*
-- Pop word from stack and store in local variable
*/
void inst_istore(void);

/*
-- Pop two words from stack; subtract the top word from the second to top word, push the answer;
*/
void inst_isub(void);

/*
-- Push constant from constant pool onto stack
*/
void inst_ldc_w(void);

/*
-- Do nothing
*/
void inst_nop(void);

/*
-- Pop word off stack and print it to standard out
*/
void inst_out(void);

/*
-- Delete word from top of stack
*/
void inst_pop(void);

/*
-- Swap the two top words on the stack
*/
void inst_swap(void);

/*
-- Pop word from stack and store in local variable ***WIDE***
*/
void inst_istore_wide(void);

/*
-- Add a constant value to a local variable. The first byte is the variable index. The second byte is the constant ***WIDE
*/
void inst_iinc_wide(void);

/*
-- Push local variable onto stack ***WIDE**
*/
void inst_iload_wide(void);

/*
-- Prefix instruction; next instruction has a 16-bit index
*/
void inst_wide(void);